package pl.com.microservices.concept.controller;

import java.math.BigInteger;

// import java.util.Arrays;
// import java.util.stream.Stream;
import java.util.Optional;
import java.util.logging.Logger;

import pl.com.microservices.concept.model.Concept;
import pl.com.microservices.concept.model.ConceptDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.dao.*;
import org.springframework.data.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
// import org.springframework.web.servlet.tags.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Controller
public class ConceptController {
   /**
    * HOW TO TEST:
    * $ mvn spring-boot:run
    * http://localhost:8087/
    * Use the following urls:
    *    /concept/create?name=[name]&score=[score]:
    *          create a new concept with an auto-generated id and price and name as passed values.
    *    /concept/delete?id=[id]:
    *          delete the concept with the passed id.
    *    /concept/update?id=[id]&email=[email]&name=[name]:
    *          update the price and the name for the concept indentified by the passed id.
    */

  @Autowired
  private ConceptDao conceptDao;

	@GetMapping(value = "/concept/{id}")
	public Concept getConcept(@PathVariable("id") /* BigInteger */ Long id) {
        Concept concept = null;
        try {
            Optional<Concept> ConceptOptional =
                    Optional.ofNullable(conceptDao.findById(BigInteger.valueOf(id)));
            if (ConceptOptional.isPresent()) {
                concept = ConceptOptional.get();;
            }
        } catch (Exception ex) {
            // @TODO @FIXME
        }
	    return concept;
	}
  
  /**
   * /create-concept  --> Create a new concept and save it in the database.
   * It is not secure operation here! There is no validation here!  See https://www.owasp.org
   * It is only for REST educational purposes...
   * 
   * @param name Concept's name
   * @param score Concept's score
   * @return A string describing if the concept is successfully created or not.
   */
  @RequestMapping("/concept/create")
  @ResponseBody
  public String create(String content, String author, String about) {
    Concept concept = null;
    try {
      concept = new Concept(content, author, about);
      conceptDao.save(concept);
    }
    catch (Exception ex) {
      return "Error while creating the concept: " + ex.toString();
    }
    return "User created succesfully!! (id = " + concept.getId() + ")";
  }
  
  /**
   * /delete-concept  --> Delete the concept having the passed id.
   * It is not secure operation here! There is no validation here!
   * It is only for REST educational purposes...
   * 
   * @param id The id of the concept to delete
   * @return A string describing if the concept is successfully deleted or not.
   */
  @RequestMapping("/concept/delete")
  @ResponseBody
  public String delete(Long id) {
    try {
      Concept concept = new Concept(id);
      conceptDao.delete(concept);
    }
    catch (Exception ex) {
      return "Error while deleting the concept: " + ex.toString();
    }
    return "Concept deleted successfully!!";
  }
  
  /**
   * /update-concept  --> Update the price and the name for the concept in the database 
   * having the passed id.
   * It is not secure operation here! There is no validation here!
   * It is only for REST educational purposes...
   * 
   * @param id The id for the concept to update.
   * @param name The new name.
   * @param score The new score.
   * @return A string describing if the concept is successfully updated or not.
   */
  @RequestMapping("/concept/update")
  @ResponseBody
  public String updateConcept(Long id, String content, String author, String about) {
    Concept concept = null;
    try {
		Optional<Concept> ConceptOptional =
                Optional.ofNullable(conceptDao.findById(BigInteger.valueOf(id)));
		if (ConceptOptional.isPresent()) {
		      concept = ConceptOptional.get();
		      concept.setContent(content);
		      concept.setAuthor(author);
		      concept.setAbout(about);
		      conceptDao.save(concept);
	    }
    } catch (Exception ex) {
      return "Error while updating the concept: " + ex.toString();
    }
    return "Concept updated successfully!!";
  }
  
}

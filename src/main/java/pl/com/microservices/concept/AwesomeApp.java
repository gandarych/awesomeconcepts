package pl.com.microservices.awesome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
@EnableAutoConfiguration
@Configuration(proxyBeanMethods = false)
@ComponentScan(basePackages = "eu.microwebservices.awesome, eu.microwebservices.awesome.model, eu.microwebservices.awesome.controller")
public class AwesomeApp {

	public static void main(String[] args) {
		SpringApplication.run(AwesomeApp.class, args);
	}

	@GetMapping("/hillo")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hillo %s! Awesome Concepts app is alive!", name);
	}

}


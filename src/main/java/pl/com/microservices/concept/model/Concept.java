package pl.com.microservices.simulation.model;

import lombok.AccessLevel;
import lombok.Setter;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

// import org.springframework.data.elasticsearch.annotations.Document;


@Data
//@Document(indexName = "conceptdb", type = "concept")
@Entity
@Table(name = "concept")
public class Concept {

   /**
    * 
// https://projectlombok.org/features/Data
// https://stormit.pl/lombok/#data
// https://medium.com/@ashish_fagna/getting-started-with-elasticsearch-creating-indices-inserting-values-and-retrieving-data-e3122e9b12c6
// https://stackoverflow.com/questions/59278195/spel-used-in-document-indexname-with-spring-data-elasticsearch-and-spring-boot
    */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @Setter(AccessLevel.PACKAGE)
    private String content;

    @Setter(AccessLevel.PACKAGE)
    private String author;

    @Setter(AccessLevel.PACKAGE)
    private String about;

    public Concept(Long id) {
        this.id = id;
    }

    public Concept(String content, String author, String about) {
        this.name = name;
        this.score = score;
        this.userid = userid;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setAbout(String about) {
        this.about = about;
    }
    
} 

CREATE TABLE concept (
	id INT PRIMARY KEY AUTO_INCREMENT,
    	content VARCHAR(255), 
    	author VARCHAR(255),
    	about VARCHAR(255)
);


INSERT INTO concept (id, content, author, about) VALUES (1, 'Good game 1', 'WILLIAM S.', 'Good'); 
INSERT INTO concept (id, content, author, about) VALUES (2, 'Testing castle 1', 'WILLIAM S.', 'Good'); 
INSERT INTO concept (id, content, author, about) VALUES (3, 'TOC', 'WILLIAM S.', 'Good'); 
INSERT INTO concept (id, content, author, about) VALUES (4, 'etc', 'WILLIAM S.', 'Good'); 
INSERT INTO concept (id, content, author, about) VALUES (5, 'Good game 2', 'WILLIAM S.', 'Good'); 
INSERT INTO concept (id, content, author, about) VALUES (6, 'My simula 1', 'WILLIAM S.', 'Good'); 
INSERT INTO concept (id, content, author, about) VALUES (7, 'My simula 1', 'WILLIAM S.', 'Good'); 
INSERT INTO concept (id, content, author, about) VALUES (8, 'My simula 1', 'WILLIAM S.', 'Good');  





#!/bin/bash

## 08_01_install_kubernetes on ubuntu
echo "RUNNING 08_01_install_kubernetes.sh"
############### Installing KUBERNETES, K8s ##############################

# Let's try to use advices at following articles to have installed 4 main tools/components:

#Docker = a container runtime. It is the component that runs your containers.
#         Support for other runtimes such as rkt is under active development in Kubernetes.
#kubectl = a CLI tool used for issuing commands to the cluster through its API Server.
#kubeadm = a CLI tool that will install and configure the various components of a cluster in a standard way.
#kubelet = a system service/program that runs on all nodes and handles node-level operations.

# Let's try to use following commands to prepare and install K8s:
echo "sudo systemctl enable docker"
sudo systemctl enable docker
echo "sudo apt install -y curl"
sudo apt install -y curl
echo "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
echo 'sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"'
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
echo "sudo apt install -y kubeadm"
sudo apt install -y kubeadm
echo "sudo kubeadm version"
sudo kubeadm version
echo "sudo swapoff -a"
sudo swapoff -a
echo "sudo hostnamectl set-hostname master-node"
sudo hostnamectl set-hostname master-node
echo "sudo hostnamectl set-hostname slave-node"
sudo hostnamectl set-hostname slave-node
echo "sudo kubeadm init --pod-network-cidr=10.244.0.0/16 >> 08_02_sudo_kubeadm_join.sh"
sudo kubeadm init --pod-network-cidr=10.244.0.0/16 >> 08_02_sudo_kubeadm_join.sh

# Your Kubernetes control-plane has initialized successfully now!
# To start using your cluster, you need to run the following as a regular user:
echo "mkdir -p $HOME/.kube"
mkdir -p $HOME/.kube
echo "sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config"
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
echo "sudo chown $(id -u):$(id -g) $HOME/.kube/config"
sudo chown $(id -u):$(id -g) $HOME/.kube/config
echo "sudo kubectl get nodes"
sudo kubectl get nodes

# You should now deploy a pod network to the cluster.
# Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
# Calico from https://kubernetes.io/docs/concepts/cluster-administration/addons/
# Install network plugin (Calico) - these now seem to leave the nodes in a "notReady" state,
# below is a fix from https://github.com/CESNET/jupyter-meta/wiki/Kubernetes-with-Kubeadm
echo "sudo kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml"
sudo kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
echo "sudo kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml"
sudo kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml
echo "sudo kubectl apply -f https://docs.projectcalico.org/v3.11/manifests/calico.yaml"
sudo kubectl apply -f https://docs.projectcalico.org/v3.11/manifests/calico.yaml
echo "sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml"
sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

# Then you can join any number of worker nodes by running the following on each as root:

#sudo kubeadm join 192.168.1.22:6443 --token wv9d86.mfssvpdndne1e96h \
#    --discovery-token-ca-cert-hash sha256:392ee523f3a93648a019880cb38f1cad7532be9a1e0edcb63e9a478d880bc33a
echo "sudo kubectl get pods --all-namespaces"
sudo kubectl get pods --all-namespaces
echo "sudo kubectl get nodes"
sudo kubectl get nodes
echo "sudo apt install -y net-tools"
sudo apt install -y net-tools
echo "ifconfig"
ifconfig
echo "ps -ef | grep kubectl"
ps -ef | grep kubectl

# Then you can join any number of worker nodes by running the following on each as root:

#sudo kubeadm join 192.168.1.22:6443 --token wv9d86.mfssvpdndne1e96h \
#    --discovery-token-ca-cert-hash sha256:392ee523f3a93648a019880cb38f1cad7532be9a1e0edcb63e9a478d880bc33a
